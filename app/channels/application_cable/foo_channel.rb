module ApplicationCable
  class FooChannel < ActionCable::Channel::Base

    # handle a new connection
    def subscribed 
      stream_from "users"
    end

    def foo
      ActionCable.server.broadcast "users", {
        message: "Hiya, your connection ID is: #{uuid}"
      }
    end
  end
end
